package ru.t1.nkiryukhin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateProfileRequest extends AbstractUserRequest {

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String middleName;

    @Nullable
    private String email;

    public UserUpdateProfileRequest(@Nullable final String token) {
        super(token);
    }

}