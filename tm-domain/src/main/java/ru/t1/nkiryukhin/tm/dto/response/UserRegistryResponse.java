package ru.t1.nkiryukhin.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.t1.nkiryukhin.tm.model.User;

@NoArgsConstructor
public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(User user) {
        super(user);
    }

}