package ru.t1.nkiryukhin.tm.data;

import org.jetbrains.annotations.NotNull;

public class UserTestData {

    @NotNull
    public final static String USUAL_USER_LOGIN = "USUAL_USER_LOGIN";

    @NotNull
    public final static String USUAL_USER_PASSWORD = "USUAL_USER_PASSWORD";

    @NotNull
    public final static String USUAL_USER_EMAIL = "USUAL_USER_EMAIL";

    @NotNull
    public final static String USUAL_FIRST_NAME = "USUAL_FIRST_NAME";

    @NotNull
    public final static String USUAL_LAST_NAME = "USUAL_LAST_NAME";

    @NotNull
    public final static String USUAL_MIDDLE_NAME = "USUAL_MIDDLE_NAME";

    @NotNull
    public final static String ADMIN_USER_LOGIN = "admin";

    @NotNull
    public final static String ADMIN_USER_PASSWORD = "sadmin";

}
