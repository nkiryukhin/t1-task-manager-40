package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.api.service.ISessionService;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.Session;

import static ru.t1.nkiryukhin.tm.data.SessionTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull static final IPropertyService propertyService = new PropertyService();

    @NotNull static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull final ISessionService service = new SessionService(connectionService);

    @Before
    public void before() {
        service.add(USUAL_SESSION1);
        service.add(USUAL_SESSION2);
    }

    @After
    public void after() throws AbstractException {
        service.remove(SESSION_LIST);
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(service.add(NULL_SESSION));
        Assert.assertNotNull(service.add(ADMIN_SESSION1));
        @Nullable final Session session = service.findOneById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void existsById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById("");
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(null);
        });
        Assert.assertFalse(service.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USUAL_SESSION1.getId()));
    }

    @Test
    public void findAll() throws UserIdEmptyException {
        Assert.assertEquals(USER_SESSION_LIST, service.findAll(USUAL_USER.getId()));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(USUAL_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USUAL_SESSION1, session);
    }

    @Test
    public void getSize() {
        int initCount = service.getSize();
        service.add(ADMIN_SESSION1);
        Assert.assertEquals(initCount + 1, service.getSize());
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final Session createdSession = service.add(ADMIN_SESSION1);
        Assert.assertTrue(service.existsById(ADMIN_SESSION1.getId()));
        service.removeOne(createdSession);
        Assert.assertFalse(service.existsById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        service.add(ADMIN_SESSION1);
        Assert.assertTrue(service.existsById(ADMIN_SESSION1.getId()));
        service.removeById(ADMIN_SESSION1.getId());
        Assert.assertFalse(service.existsById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void remove() throws AccessDeniedException, UserIdEmptyException {
        int count = service.getSize();
        service.remove(SESSION_LIST);
        Assert.assertEquals(count - 2, service.getSize());
    }

}

