package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IProjectService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.*;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.Project;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull static final IPropertyService propertyService = new PropertyService();

    @NotNull static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull static final IProjectService service = new ProjectService(connectionService);

    @Before
    public void before() {
        service.add(USUAL_PROJECT1);
        service.add(USUAL_PROJECT2);
    }

    @After
    public void after() throws AbstractException {
        service.remove(PROJECT_LIST);
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(service.add(NULL_PROJECT));
        Assert.assertNotNull(service.add(ADMIN_PROJECT1));
        @Nullable final Project project = service.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void changeProjectStatusById() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById(null, USUAL_PROJECT1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById("", USUAL_PROJECT1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(USUAL_USER.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(USUAL_USER.getId(), "", status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeProjectStatusById(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null);
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.changeProjectStatusById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, status);
        });
        service.changeProjectStatusById(USUAL_USER.getId(), USUAL_PROJECT1.getId(), status);
        Project updatedProject = service.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertEquals(status, updatedProject.getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        Project project = service.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertNotNull(project);
        final int index = service.findAll(USUAL_USER.getId()).indexOf(project);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusByIndex(null, index, status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusByIndex("", index, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeProjectStatusByIndex(USUAL_USER.getId(), null, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeProjectStatusByIndex(USUAL_USER.getId(), -1, status);
        });
        service.changeProjectStatusByIndex(USUAL_USER.getId(), index, status);
        project = service.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clearByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        service.clear(USUAL_USER.getId());
        Assert.assertTrue(service.findAll(USUAL_USER.getId()).isEmpty());
    }

    @Test
    public void create() throws AbstractFieldException {
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_USER.getId(), "", null);
        });
        @NotNull final Project project = service.create(ADMIN_USER.getId(), ADMIN_PROJECT1.getName(), "desc");
        Assert.assertEquals(project, service.findOneById(ADMIN_USER.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_USER.getId(), project.getUserId());
        PROJECT_LIST.add(project); //добавить в список новый проект, чтобы его удалить в after()
    }

    @Test
    public void existsById() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USUAL_USER.getId(), "");
        });
        Assert.assertFalse(service.existsById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(USUAL_USER.getId(), USUAL_PROJECT1.getId()));
    }

    @Test
    public void findAll() throws UserIdEmptyException {
        List<Project> projects = service.findAll(USUAL_USER.getId());
        projects.sort(NameComparator.INSTANCE);
        Assert.assertEquals(USUAL_PROJECT_LIST, projects);
    }

    @Test
    public void findAllByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        List<Project> foundProjects = service.findAll(USUAL_USER.getId());
        foundProjects.sort(NameComparator.INSTANCE);
        Assert.assertEquals(USUAL_PROJECT_LIST, foundProjects);
    }

    @Test
    public void findAllComparatorByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            service.findAll("", comparatorInner);
        });
        Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(
                USUAL_PROJECT_LIST.stream().sorted(comparator).collect(Collectors.toList()),
                service.findAll(USUAL_USER.getId(), comparator)
        );
    }

    @Test
    public void findAllSortByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            service.findAll("", sortInner);
        });
        Sort sort = Sort.BY_NAME;
        Assert.assertEquals(
                USUAL_PROJECT_LIST.stream().sorted(sort.getComparator()).collect(Collectors.toList()),
                service.findAll(USUAL_USER.getId(), sort.getComparator())
        );
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USUAL_PROJECT1.getId());
        });
        Assert.assertNull(service.findOneById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USUAL_PROJECT1, project);
    }

    @Test
    public void findOneByIndex() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USUAL_USER.getId(), -1);
        });
        final int index = service.findAll(USUAL_USER.getId()).indexOf(USUAL_PROJECT1);
        @Nullable final Project project = service.findOneByIndex(USUAL_USER.getId(), index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USUAL_PROJECT1, project);
    }

    @Test
    public void getSize() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize("");
        });
        Assert.assertEquals(0, service.getSize(ADMIN_USER.getId()));
        service.add(ADMIN_PROJECT1);
        Assert.assertEquals(1, service.getSize(ADMIN_USER.getId()));
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final Project createdProject = service.add(ADMIN_PROJECT1);
        Assert.assertNotNull(service.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
        service.removeOne(createdProject);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USUAL_USER.getId(), "");
        });
        @Nullable final Project createdProject = service.add(ADMIN_PROJECT1);
        service.removeById(ADMIN_USER.getId(), createdProject.getId());
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractException {
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(ADMIN_USER.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(ADMIN_USER.getId(),-1);
        });
        @Nullable final Project createdProject = service.add(ADMIN_PROJECT1);
        final int index = service.findAll(ADMIN_USER.getId()).indexOf(createdProject);
        service.removeByIndex(ADMIN_USER.getId(), index);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeAll() throws AccessDeniedException, UserIdEmptyException {
        int countProjects = service.getSize(USUAL_USER.getId());
        service.removeAll(USUAL_USER.getId());
        Assert.assertEquals(2, countProjects - service.getSize(USUAL_USER.getId()));
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USUAL_PROJECT1.getId(), USUAL_PROJECT1.getName(), USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USUAL_PROJECT1.getId(), USUAL_PROJECT1.getName(), USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), null, USUAL_PROJECT1.getName(), USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), "", USUAL_PROJECT1.getName(), USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null, USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), USUAL_PROJECT1.getId(), "", USUAL_PROJECT1.getDescription());
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.updateById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, USUAL_PROJECT1.getName(), USUAL_PROJECT1.getDescription());
        });
        @NotNull final String name = USUAL_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USUAL_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        service.updateById(USUAL_USER.getId(), USUAL_PROJECT1.getId(), name, description);
        Project updatedProject = service.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertEquals(name, updatedProject.getName());
        Assert.assertEquals(description, updatedProject.getDescription());
    }

}

