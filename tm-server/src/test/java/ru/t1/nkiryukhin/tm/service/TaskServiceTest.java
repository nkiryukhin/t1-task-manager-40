package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.api.service.ITaskService;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.TaskNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.*;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.USUAL_PROJECT1;
import static ru.t1.nkiryukhin.tm.data.TaskTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull static final IPropertyService propertyService = new PropertyService();

    @NotNull static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull static final ITaskService service = new TaskService(connectionService);

    @Before
    public void before() {
        service.add(USUAL_TASK1);
        service.add(USUAL_TASK2);
    }

    @After
    public void after() throws AbstractException {
        service.remove(TASK_LIST);
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(service.add(ADMIN_USER.getId(), NULL_TASK));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, ADMIN_TASK1);
        });
        Assert.assertNotNull(service.add(ADMIN_USER.getId(), ADMIN_TASK1));
        @Nullable final Task task = service.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(ADMIN_TASK1, task);
    }

    @Test
    public void changeTaskStatusById() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById(null, USUAL_TASK1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById("", USUAL_TASK1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(USUAL_USER.getId(), null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(USUAL_USER.getId(), "", status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeTaskStatusById(USUAL_USER.getId(), USUAL_TASK1.getId(), null);
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.changeTaskStatusById(USUAL_USER.getId(), NON_EXISTING_TASK_ID, status);
        });
        service.changeTaskStatusById(USUAL_USER.getId(), USUAL_TASK1.getId(), status);
        Task updatedTask = service.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertEquals(status, updatedTask.getStatus());
    }

    @Test
    public void changeTaskStatusByIndex() throws AbstractException {
        @NotNull final Status status = Status.COMPLETED;
        final int index = service.findAll(USUAL_USER.getId()).indexOf(USUAL_TASK1);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusByIndex(null, index, status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusByIndex("", index, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeTaskStatusByIndex(USUAL_USER.getId(), null, status);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeTaskStatusByIndex(USUAL_USER.getId(), -1, status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeTaskStatusByIndex(USUAL_USER.getId(), index, null);
        });
        service.changeTaskStatusByIndex(USUAL_USER.getId(), index, status);

        Task task = service.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void clearByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        service.clear(USUAL_USER.getId());
        Assert.assertEquals(0, service.getSize(USUAL_USER.getId()));
    }

    @Test
    public void existsById() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_TASK_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USUAL_USER.getId(), "");
        });
        Assert.assertFalse(service.existsById(USUAL_USER.getId(), NON_EXISTING_TASK_ID));
        Assert.assertTrue(service.existsById(USUAL_USER.getId(), USUAL_TASK1.getId()));
    }

    @Test
    public void findAll() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        Assert.assertEquals(USUAL_TASK_LIST, service.findAll(USUAL_USER.getId()));
    }

    @Test
    public void findAllComparatorByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            service.findAll("", comparatorInner);
        });
        Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USUAL_TASK_LIST.stream().sorted(comparator).collect(Collectors.toList()), service.findAll(USUAL_USER.getId(), comparator));
    }

    @Test
    public void findAllSortByUserId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            service.findAll("", sortInner);
        });
        Sort sort = Sort.BY_NAME;
        Assert.assertEquals(USUAL_TASK_LIST.stream().sorted(sort.getComparator()).collect(Collectors.toList()), service.findAll(USUAL_USER.getId(), sort.getComparator()));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USUAL_TASK1.getId());
        });
        Assert.assertNull(service.findOneById(USUAL_USER.getId(), NON_EXISTING_TASK_ID));
        @Nullable final Task task = service.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void findOneByIndexByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(USUAL_USER.getId(), -1);
        });
        final int index = service.findAll(USUAL_USER.getId()).indexOf(USUAL_TASK1);
        @Nullable final Task task = service.findOneByIndex(USUAL_USER.getId(), index);
        Assert.assertNotNull(task);
        Assert.assertEquals(USUAL_TASK1, task);
    }

    @Test
    public void getSize() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize("");
        });
        Assert.assertEquals(0, service.getSize(ADMIN_USER.getId()));
        service.add(ADMIN_TASK1);
        Assert.assertEquals(1, service.getSize(ADMIN_USER.getId()));
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        Assert.assertNotNull(service.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
        service.removeOne(createdTask);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractFieldException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USUAL_USER.getId(), "");
        });
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        Assert.assertTrue(service.existsById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
        service.removeById(ADMIN_USER.getId(), createdTask.getId());
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByIndexByUserId() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex("", null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(USUAL_USER.getId(), -1);
        });
        @Nullable final Task createdTask = service.add(ADMIN_TASK1);
        final int index = service.findAll(ADMIN_USER.getId()).indexOf(createdTask);
        service.removeByIndex(ADMIN_USER.getId(), index);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void removeAll() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            String userId = null;
            service.removeAll(userId);
        });
        Assert.assertTrue(service.getSize(USUAL_USER.getId()) > 0);
        service.removeAll(USUAL_USER.getId());
        Assert.assertTrue(service.getSize(USUAL_USER.getId()) == 0);
    }

    @Test
    public void create() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, ADMIN_TASK1.getName(), null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", ADMIN_TASK1.getName(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_USER.getId(), null, null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create(ADMIN_USER.getId(), "", null);
        });
        @NotNull final Task task = service.create(ADMIN_USER.getId(), ADMIN_TASK1.getName(), "name");
        Assert.assertEquals(task, service.findOneById(ADMIN_USER.getId(), task.getId()));
        Assert.assertEquals(ADMIN_TASK1.getName(), task.getName());
        Assert.assertEquals(ADMIN_USER.getId(), task.getUserId());
        TASK_LIST.add(task);
    }

    @Test
    public void findAllByProjectId() throws UserIdEmptyException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAllByProjectId(null, USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAllByProjectId("", USUAL_PROJECT1.getId());
        });
        @NotNull final Collection<Task> emptyCollection = Collections.emptyList();
        Assert.assertEquals(emptyCollection, service.findAllByProjectId(USUAL_USER.getId(), null));
        Assert.assertEquals(emptyCollection, service.findAllByProjectId(USUAL_USER.getId(), ""));
        Assert.assertEquals(USUAL_TASK_LIST, service.findAllByProjectId(USUAL_USER.getId(), USUAL_PROJECT1.getId()));
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, USUAL_TASK1.getId(), USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", USUAL_TASK1.getId(), USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), null, USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), "", USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), USUAL_TASK1.getId(), null, USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(USUAL_USER.getId(), USUAL_TASK1.getId(), "", USUAL_TASK1.getDescription());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.updateById(USUAL_USER.getId(), NON_EXISTING_TASK_ID, USUAL_TASK1.getName(), USUAL_TASK1.getDescription());
        });
        @NotNull final String name = USUAL_TASK1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = USUAL_TASK1.getDescription() + NON_EXISTING_TASK_ID;
        service.updateById(USUAL_USER.getId(), USUAL_TASK1.getId(), name, description);
        Task updatedTask = service.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertEquals(name, updatedTask.getName());
        Assert.assertEquals(description, updatedTask.getDescription());
    }

}
