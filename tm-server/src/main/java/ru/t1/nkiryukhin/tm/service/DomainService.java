package ru.t1.nkiryukhin.tm.service;

import lombok.SneakyThrows;
import ru.t1.nkiryukhin.tm.api.service.IDomainService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public final class DomainService implements IDomainService {

    private final IPropertyService propertyService;

    public DomainService(IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @SneakyThrows
    public void dropDatabase(final String passphrase) {
        if (passphrase == null || !passphrase.equals(propertyService.getDbaPassphrase())) throw new AccessDeniedException();
        String urlDb = propertyService.getDatabaseUrl();
        String databaseName = propertyService.getDatabaseName();
        String username = propertyService.getDatabaseUser();
        String password = propertyService.getDatabasePassword();
        String urlServer = propertyService.getDatabaseServerUrl();

        //предварительно убиваем все сессии, чтобы можно было дропнуть базу
        Connection connection = DriverManager.getConnection(urlDb, username, password);
        Statement statement = connection.createStatement();
        String sqlKillAllSessions = "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() " +
                "AND datname = current_database();";
        statement.execute(sqlKillAllSessions);
        connection.close();

        connection = DriverManager.getConnection(urlServer, username, password);
        statement = connection.createStatement();
        String sqlDropDB = "DROP DATABASE " + databaseName;
        statement.executeUpdate(sqlDropDB);
        connection.close();
    }

    @SneakyThrows
    public void createDatabase() {
        String username = propertyService.getDatabaseUser();
        String password = propertyService.getDatabasePassword();
        String urlServer = propertyService.getDatabaseServerUrl();
        String databaseName = propertyService.getDatabaseName();
        String urlDb = propertyService.getDatabaseUrl();

        //создаём объект БД
        Connection connection = DriverManager.getConnection(urlServer, username, password);
        Statement statement = connection.createStatement();
        String sqlCreateDB = "CREATE DATABASE " + databaseName + " WITH OWNER = postgres " +
                "ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251' " +
                "TABLESPACE = pg_default CONNECTION LIMIT = -1 IS_TEMPLATE = False;";
        statement.executeUpdate(sqlCreateDB);
        connection.close();

        //подключаемся к созданной БД и добавляем в неё кастомные типы и таблицы
        connection = DriverManager.getConnection(urlDb, username, password);
        statement = connection.createStatement();
        String sqlCreateTypes = "CREATE TYPE public.role AS ENUM\n" +
                "    ('USUAL', 'ADMIN');\n" +
                "ALTER TYPE public.role\n" +
                "    OWNER TO postgres;\n" +
                "CREATE TYPE public.status AS ENUM\n" +
                "    ('NOT_STARTED', 'IN_PROGRESS', 'COMPLETED');\n" +
                "ALTER TYPE public.status\n" +
                "    OWNER TO postgres;";
        statement.executeUpdate(sqlCreateTypes);
        String sqlCreateTables = "CREATE TABLE IF NOT EXISTS public.tm_project\n" +
                "(\n" +
                "    id text COLLATE pg_catalog.\"default\" NOT NULL,\n" +
                "    created timestamp without time zone,\n" +
                "    name text COLLATE pg_catalog.\"default\",\n" +
                "    description text COLLATE pg_catalog.\"default\",\n" +
                "    user_id text COLLATE pg_catalog.\"default\",\n" +
                "    status status,\n" +
                "    CONSTRAINT tm_project_pkey PRIMARY KEY (id)\n" +
                ")\n" +
                "TABLESPACE pg_default;\n" +
                "ALTER TABLE IF EXISTS public.tm_project\n" +
                "    OWNER to postgres;\n" +
                "-- Table: public.tm_session\n" +
                "CREATE TABLE IF NOT EXISTS public.tm_session\n" +
                "(\n" +
                "    id text COLLATE pg_catalog.\"default\" NOT NULL,\n" +
                "    created timestamp without time zone,\n" +
                "    user_id text COLLATE pg_catalog.\"default\",\n" +
                "    role role,\n" +
                "    CONSTRAINT tm_session_pkey PRIMARY KEY (id)\n" +
                ")\n" +
                "TABLESPACE pg_default;\n" +
                "ALTER TABLE IF EXISTS public.tm_session\n" +
                "    OWNER to postgres;\n" +
                "-- Table: public.tm_task\n" +
                "CREATE TABLE IF NOT EXISTS public.tm_task\n" +
                "(\n" +
                "    id text COLLATE pg_catalog.\"default\" NOT NULL,\n" +
                "    created timestamp without time zone,\n" +
                "    name text COLLATE pg_catalog.\"default\",\n" +
                "    description text COLLATE pg_catalog.\"default\",\n" +
                "    user_id text COLLATE pg_catalog.\"default\",\n" +
                "    status status,\n" +
                "    project_id text COLLATE pg_catalog.\"default\",\n" +
                "    CONSTRAINT tm_task_pkey PRIMARY KEY (id)\n" +
                ")\n" +
                "TABLESPACE pg_default;\n" +
                "ALTER TABLE IF EXISTS public.tm_task\n" +
                "    OWNER to postgres;\n" +
                "-- Table: public.tm_user\n" +
                "CREATE TABLE IF NOT EXISTS public.tm_user\n" +
                "(\n" +
                "    id text COLLATE pg_catalog.\"default\" NOT NULL,\n" +
                "    login text COLLATE pg_catalog.\"default\",\n" +
                "    password text COLLATE pg_catalog.\"default\",\n" +
                "    email text COLLATE pg_catalog.\"default\",\n" +
                "    locked boolean,\n" +
                "    first_name text COLLATE pg_catalog.\"default\",\n" +
                "    last_name text COLLATE pg_catalog.\"default\",\n" +
                "    middle_name text COLLATE pg_catalog.\"default\",\n" +
                "    role role,\n" +
                "    CONSTRAINT tm_user_pkey PRIMARY KEY (id),\n" +
                "    CONSTRAINT login_unique UNIQUE (login)\n" +
                ")\n" +
                "TABLESPACE pg_default;\n" +
                "ALTER TABLE IF EXISTS public.tm_user\n" +
                "    OWNER to postgres;";
        statement.executeUpdate(sqlCreateTables);
        connection.close();
    }

}
