package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.model.Project;


public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws AbstractException;

    @NotNull
    Project changeProjectStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws AbstractException;

    void clear(@Nullable String userId) throws UserIdEmptyException;

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractFieldException;

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

}